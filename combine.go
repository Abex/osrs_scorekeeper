package main

import (
	"io/ioutil"
	"os"
	"strings"
	"unicode"
)

var Skills = []string{
	"Overall",
	"Attack",
	"Defence",
	"Strength",
	"Hitpoints",
	"Ranged",
	"Prayer",
	"Magic",
	"Cooking",
	"Woodcutting",
	"Fletching",
	"Fishing",
	"Firemaking",
	"Crafting",
	"Smithing",
	"Mining",
	"Herblore",
	"Agility",
	"Thieving",
	"Slayer",
	"Farming",
	"Runecraft",
	"Hunter",
	"Construction",
	"EHP",
}

func skillID(name string) int {
	for i := range Skills {
		if Skills[i] == name {
			return i
		}
	}
	return -1
}

var EHP = skillID("EHP")
var NumRSSkills = skillID("Construction") + 1

const NA = -1

type Skill struct {
	Name  string
	XP    int
	Level int
	Rank  int
}

type User struct {
	Name    string
	FmtName string
	Email   string
	ID      int
	Skills  []Skill
}

type Store struct {
	Users  map[string]int
	Skills [][]int
}

func GetUsers(store *Store) (map[int]User, error) {
	name2mail, err := func() ([][2]string, error) {
		ifi, err := os.OpenFile("emails.tsv", os.O_CREATE|os.O_RDONLY, 0777)
		if err != nil {
			return nil, err
		}
		b, err := ioutil.ReadAll(ifi)
		if err != nil {
			return nil, err
		}
		lines := strings.Split(string(b), "\n")
		out := make([][2]string, 0, len(lines))
		for _, line := range lines {
			if len(line) > 3 {
				ln := strings.SplitN(line, "\t", 2)
				out = append(out, [2]string{
					ln[0],
					ln[1],
				})
			}
		}
		return out, nil
	}()
	if err != nil {
		return nil, err
	}
	names, ehps, err := GetCML()
	if err != nil {
		return nil, err
	}
	ehprank := make([]int, len(ehps))
	for i := range ehprank {
		ehprank[i] = i + 1
	}

	users := map[int]User{}

	for i, name := range names {
		name = strings.ToLower(name)
		id, ok := store.Users[name]
		if !ok {
			oldname, err := GetPreviousName(name)
			if err != nil {
				return nil, err
			}
			if oldname != "" {
				id, ok = store.Users[oldname]
				if ok {
					store.Users[name] = id
					delete(store.Users, oldname)
					for i := range name2mail {
						if strings.ToLower(name2mail[i][0]) == oldname {
							name2mail[i][0] = name
						}
					}
				}
			}
			if !ok { // new User
				for _, iid := range store.Users {
					if id < iid {
						id = iid
					}
				}
				id++
				store.Users[name] = id
				name2mail = append(name2mail, [2]string{name, "?"})
			}
		}
		email := "?"
		for _, row := range name2mail {
			if strings.ToLower(row[0]) == name {
				email = row[1]
				break
			}
		}
		skills := make([]Skill, len(Skills))
		for i := range skills {
			skills[i].Name = Skills[i]
		}

		rshs, err := GetRSUser(id, name)
		if err != nil {
			return nil, err
		}
		for i := 0; i < NumRSSkills; i++ {
			skills[i].Rank = rshs[i][0]
			skills[i].Level = rshs[i][1]
			skills[i].XP = rshs[i][2]
		}

		skills[EHP].XP = int(ehps[i] * 1000)
		rank := 1
		for _, ehp := range ehps {
			if ehp > ehps[i] {
				rank++
			}
		}
		skills[EHP].Rank = rank
		nextCaps := true
		fmtname := strings.Map(func(v rune) rune {
			if v == '_' {
				v = ' '
			}
			if nextCaps {
				v = unicode.ToTitle(v)
			}
			nextCaps = v == ' '
			return v
		}, name)
		users[id] = User{
			FmtName: fmtname,
			Name:    name,
			Email:   email,
			ID:      id,
			Skills:  skills,
		}
	}

	err = func() error {
		fi, err := os.OpenFile("emails.tsv", os.O_CREATE|os.O_WRONLY|os.O_TRUNC, 0777)
		if err != nil {
			return err
		}
		defer fi.Close()
		for _, names := range name2mail {
			_, err = fi.WriteString(names[0])
			if err != nil {
				return err
			}
			_, err = fi.WriteString("\t")
			if err != nil {
				return err
			}
			_, err = fi.WriteString(names[1])
			if err != nil {
				return err
			}
			_, err = fi.WriteString("\n")
			if err != nil {
				return err
			}
		}
		return nil
	}()
	if err != nil {
		return nil, err
	}
	return users, nil
}
