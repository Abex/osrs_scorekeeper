package main

import (
	"bytes"
	"encoding/json"
	"flag"
	"fmt"
	"html/template"
	"io"
	"os"
	"sort"
	"text/tabwriter"

	"gopkg.in/gomail.v2"

	"bitbucket.org/abex/yumeko/conf"
	"bitbucket.org/abex/yumeko/glug"
)

var log = glug.NewLogger("main")

var emailHost string
var emailPort int
var emailPassword string
var emailUsername string
var emailSender string

func init() {
	conf.Config(&emailHost, "email", `
# SMTP serer Hostname
host = mail.example.com
`)
	conf.Config(&emailPort, "email", `
# SMTP server port
port = 587
`)
	conf.Config(&emailUsername, "email", `
# SMTP server username
username = user@example.com
`)
	conf.Config(&emailPassword, "email", `
# SMTP server password
password = SoSecureL0L
`)
	conf.Config(&emailSender, "email", `
# Email to send from 
sender = user@example.com
`)
}

func comma(v int) string {
	if v == 0 {
		return "0"
	}
	out := []byte{}
	for i := 0; ; i++ {
		out = append(out, (byte)('0'+(v%10)))
		v = v / 10
		if v == 0 {
			break
		}
		if i%3 == 2 {
			out = append(out, ',')
		}
	}
	last := len(out) - 1
	for i := 0; i < len(out)/2; i++ {
		out[i], out[last-i] = out[last-i], out[i]
	}
	return string(out)
}

func main() {
	//flags
	def := "./"
	configPtr := flag.String("config", def, def)
	compare := flag.Bool("cmp", false, "Print compare debug and exit")
	live := flag.Bool("live", false, "Send e-mails")
	commit := flag.Bool("commit", false, "Commit to store, even if not sending")
	flag.Parse()

	//config
	_, err := os.Stat(*configPtr)
	if os.IsNotExist(err) {
		err = os.MkdirAll(*configPtr, 0770)
		if err != nil {
			panic(err)
		}
	}
	conf.Load(*configPtr)

	passedTemplate := template.New("passed")
	passingTemplate := template.New("passing")
	if *live {
		passedTemplate.Funcs(map[string]interface{}{
			"comma": comma,
		})
		passedTemplate, err = passedTemplate.Parse(`
<h2> Oldschool Runescape Scorekeeper </h2>
<b> {{.From.Name}} </b> has surpassed your rank in <b> {{ index .Skills .SkillID }} </b><br>

<style>
tr>td:nth-child(even){background:rgba(127,127,127,.2);}
td{padding:0 .2em;}
table{border-spacing:0;}
</style>

<table>
	<tr>
		<td></td>
		{{if .ShowLevel}}<td> Level </td>{{end}}
		{{if .ShowRank}}<td> Rank </td>{{end}}
		{{if .ShowXP}}<td> Experience </td>{{end}}
	</tr>
	{{ $ToSkill := index .To.Skills .SkillID }}
	{{ $FromSkill := index .From.Skills .SkillID }}
	<tr>
		<td> {{.To.FmtName}} </td>
		{{if .ShowLevel}}<td> {{$ToSkill.Level}} </td>{{end}}
		{{if .ShowRank}}<td> {{comma $ToSkill.Rank}} </td>{{end}}
		{{if .ShowXP}}<td> {{comma $ToSkill.XP}} </td>{{end}}
	</tr>
	<tr>
		<td> {{.From.FmtName}} </td>
		{{if .ShowLevel}}<td> {{$FromSkill.Level}} </td>{{end}}
		{{if .ShowRank}}<td> {{comma $FromSkill.Rank}} </td>{{end}}
		{{if .ShowXP}}<td> {{comma $FromSkill.XP}} </td>{{end}}
	</tr>
</table>
		`)
		if err != nil {
			panic(err)
		}
		passingTemplate.Funcs(map[string]interface{}{
			"comma": comma,
		})
		passingTemplate, err = passingTemplate.Parse(`
<h2> Oldschool Runescape Scorekeeper </h2>
You have surpassed <b>{{.NameConcat}}</b>'s rank in <b> {{ index .Skills .SkillID }} </b><br>

<style>
tr>td:nth-child(even){background:rgba(127,127,127,.2);}
td{padding:0 .2em;}
table{border-spacing:0;}
</style>

<table>
	<tr>
		<td></td>
		{{if .ShowLevel}}<td> Level </td>{{end}}
		{{if .ShowRank}}<td> Rank </td>{{end}}
		{{if .ShowXP}}<td> Experience </td>{{end}}
	</tr>
	{{ $FromSkill := index .From.Skills .SkillID }}
	<tr>
		<td> {{.From.FmtName}} </td>
		{{if .ShowLevel}}<td> {{$FromSkill.Level}} </td>{{end}}
		{{if .ShowRank}}<td> {{comma $FromSkill.Rank}} </td>{{end}}
		{{if .ShowXP}}<td> {{comma $FromSkill.XP}} </td>{{end}}
	</tr>
	{{ range $_, $iTo := .To }}
		{{ $To := index $.Users $iTo }}
		{{ $ToSkill := index $To.Skills $.SkillID }}
		<tr>
			<td> {{$To.FmtName}} </td>
			{{if $.ShowLevel}}<td> {{$ToSkill.Level}} </td>{{end}}
			{{if $.ShowRank}}<td> {{comma $ToSkill.Rank}} </td>{{end}}
			{{if $.ShowXP}}<td> {{comma $ToSkill.XP}} </td>{{end}}
		</tr>
	{{end}}
</table>
		`)
		if err != nil {
			panic(err)
		}
	}

	//Read + init store
	store, err := func() (Store, error) {
		var store Store
		ifi, err := os.OpenFile("store.json", os.O_CREATE|os.O_RDONLY, 0777)
		if err != nil {
			return store, err
		}
		defer ifi.Close()
		err = json.NewDecoder(ifi).Decode(&store)
		if err == io.EOF {
			err = nil
		}
		if store.Users == nil {
			store.Users = map[string]int{}
		}
		if store.Skills == nil {
			store.Skills = [][]int{}
		}
		return store, err
	}()
	if err != nil {
		panic(err)
	}

	//Get user data cml+hiscores
	users, err := GetUsers(&store)
	if err != nil {
		panic(err)
	}
	log.Info("Retrived")

	//Write table to console
	if *compare {
		w := tabwriter.NewWriter(os.Stdout, 3, 0, 0, ' ', tabwriter.Debug|tabwriter.AlignRight)
		fmt.Fprint(w, "Name\tID\tEmail")
		for _, skill := range Skills {
			fmt.Fprintf(w, "\t%v", skill)
		}
		fmt.Fprint(w, "\n")
		for _, user := range users {
			fmt.Fprintf(w, "%v\t%v\t%v", user.Name, user.ID, user.Email)
			for _, skill := range user.Skills {
				fmt.Fprintf(w, "\t%v", skill.Rank)
			}
			fmt.Fprint(w, "\n")
		}
		w.Flush()
	} else {
		//Do compareison
		var oldSkills [][]int
		dontCommit := !(*live || *commit)
		if dontCommit {
			oldSkills = make([][]int, len(store.Skills))
			copy(oldSkills, store.Skills)
		}
		allChanges := []Change{}
		//Add new skills
		for len(store.Skills) < len(Skills) {
			store.Skills = append(store.Skills, []int{})
		}
		for i, skill := range store.Skills {
			ranking := make([][2]int, 0, len(users))
			//remove removed users
			for _, user := range skill {
				_, ok := users[user]
				if ok {
					ranking = append(ranking, [2]int{user, 0})
				}
			}

			//get current user lists
			allUsers := make([]int, 0, len(users))
			oldUsers := make([]int, 0, len(ranking))
			for id := range users {
				found := false
				for _, aid := range ranking {
					found = found || aid[0] == id
				}
				if found {
					oldUsers = append(oldUsers, id)
				}
				allUsers = append(allUsers, id)
			}
			//sort them
			sort.Stable(&userSort{
				Skill: i,
				Mapp:  oldUsers,
				Users: users,
			})
			if len(allUsers) != len(oldUsers) {
				sort.Stable(&userSort{
					Skill: i,
					Mapp:  allUsers,
					Users: users,
				})
			} else {
				allUsers = oldUsers
			}
			//Add to the other array
			for i := range oldUsers {
				ranking[i][1] = oldUsers[i]
			}
			//compare
			changes, err := DetectChanges(ranking)
			if err != nil {
				panic(err)
			}
			for ci := range changes {
				changes[ci].Skill = i
			}
			allChanges = append(allChanges, changes...)

			store.Skills[i] = allUsers
		}
		if dontCommit {
			store.Skills = oldSkills
		}
		var s gomail.SendCloser
		if *live {
			log.Info("Sending emails")
			s, err = gomail.NewDialer(emailHost, emailPort, emailUsername, emailPassword).Dial()
			if err != nil {
				panic(err)
			}
		}
		m := gomail.NewMessage()
		for _, change := range allChanges {
			from := users[change.Higher]
			fmt.Printf("%v passed ", from.Name)

			//Congratulations
			nameConcat := &bytes.Buffer{}
			for i := 0; i < len(change.Displaced)-2; i++ {
				nameConcat.WriteString(users[change.Displaced[i]].FmtName)
				nameConcat.WriteString(", ")
			}
			if len(change.Displaced) > 1 {
				nameConcat.WriteString(users[change.Displaced[len(change.Displaced)-2]].FmtName)
				nameConcat.WriteString(" and ")
			}
			nameConcat.WriteString(users[change.Displaced[len(change.Displaced)-1]].FmtName)
			nameConcatString := string(nameConcat.Bytes())
			if *live {
				m.SetAddressHeader("From", emailSender, nameConcatString)
				m.SetAddressHeader("To", from.Email, from.FmtName)
				m.SetHeader("Subject", fmt.Sprintf("OSRS Scoreboard Alert - You have surpassed %v's rank in %v", nameConcatString, Skills[change.Skill]))
				buf := &bytes.Buffer{}
				err = passingTemplate.Execute(buf, struct {
					From       User
					To         []int
					NameConcat string
					Users      map[int]User
					Skills     []string
					SkillID    int
					ShowLevel  bool
					ShowRank   bool
					ShowXP     bool
				}{
					From:       from,
					To:         change.Displaced,
					NameConcat: nameConcatString,
					Users:      users,
					Skills:     Skills,
					SkillID:    change.Skill,
					ShowLevel:  change.Skill != EHP,
					ShowRank:   change.Skill != EHP,
					ShowXP:     true,
				})
				if err != nil {
					panic(err)
				}
				m.SetBody("text/html", string(buf.Bytes()))
				gomail.Send(s, m)
				m.Reset()
			}
			for _, passed := range change.Displaced { //anti-congrats
				to := users[passed]
				fmt.Printf("%v ", to.Name)
				if *live {
					m.SetAddressHeader("From", emailSender, from.FmtName)
					m.SetAddressHeader("To", to.Email, to.FmtName)
					m.SetHeader("Subject", fmt.Sprintf("OSRS Scoreboard Alert - %v has surpassed your rank in %v", from.FmtName, Skills[change.Skill]))
					buf := &bytes.Buffer{}
					err = passedTemplate.Execute(buf, struct {
						From      User
						To        User
						Skills    []string
						SkillID   int
						ShowLevel bool
						ShowRank  bool
						ShowXP    bool
					}{
						From:      from,
						To:        to,
						Skills:    Skills,
						SkillID:   change.Skill,
						ShowLevel: change.Skill != EHP,
						ShowRank:  change.Skill != EHP,
						ShowXP:    true,
					})
					if err != nil {
						panic(err)
					}
					m.SetBody("text/html", string(buf.Bytes()))
					gomail.Send(s, m)
					m.Reset()
				}
			}
			fmt.Printf("in skill %v\n", Skills[change.Skill])
		}
	}
	log.Info("Saving")

	err = func() error {
		fi, err := os.OpenFile("store.json", os.O_CREATE|os.O_WRONLY|os.O_TRUNC, 0777)
		if err != nil {
			return err
		}
		defer fi.Close()
		return json.NewEncoder(fi).Encode(store)
	}()
	if err != nil {
		panic(err)
	}
}

type userSort struct {
	Skill int
	Mapp  []int
	Users map[int]User
}

func (u *userSort) Len() int {
	return len(u.Mapp)
}
func (u *userSort) Swap(a, b int) {
	u.Mapp[a], u.Mapp[b] = u.Mapp[b], u.Mapp[a]
}
func (u *userSort) Less(a, b int) bool {
	return u.Users[u.Mapp[a]].Skills[u.Skill].Rank < u.Users[u.Mapp[b]].Skills[u.Skill].Rank
}
