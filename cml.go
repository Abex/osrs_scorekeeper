package main

import (
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"strconv"
	"strings"

	"bitbucket.org/abex/yumeko/conf"
	"github.com/pkg/errors"
)

var cmlid string

func init() {
	conf.Config(&cmlid, "main", `
# CML group id 
group = 10
`)
}

func GetPreviousName(name string) (string, error) {
	res, err := http.Get(fmt.Sprintf("http://crystalmathlabs.com/tracker/api.php?type=previousname&player=%v", name))
	if err != nil {
		return "", errors.WithStack(err)
	}
	defer res.Body.Close()
	b, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return "", errors.WithStack(err)
	}
	name = strings.TrimSpace(string(b))
	if name == "-1" {
		return "", nil
	}
	return name, nil
}

func GetCML() ([]string, []float64, error) {
	name := fmt.Sprintf("group%v", cmlid)
	res, err := GetHTTPOrCache(name, fmt.Sprintf("https://crystalmathlabs.com/tracker/api.php?type=virtualhiscores&page=timeplayed&skill=ehp&group=%v", cmlid))
	if err != nil {
		return nil, nil, errors.WithStack(err)
	}
	defer func() { res.Close() }()
	names, ehps, err := parseCML(res)
	if err != nil {
		var cerr error
		res, cerr = res.GetCached()
		if cerr == nil {
			names, ehps, err = parseCML(res)
		}
	}
	return names, ehps, err
}

func parseCML(res io.Reader) ([]string, []float64, error) {
	b, err := ioutil.ReadAll(res)
	if err != nil {
		return nil, nil, errors.WithStack(err)
	}
	s := string(b)
	num, err := strconv.Atoi(s)
	if err == nil {
		return nil, nil, fmt.Errorf("CML Erorr: %v", num)
	}
	s = strings.TrimPrefix(s, "\ufeff")
	ss := strings.Split(s, "\n")
	if len(ss) < 2 {
		return nil, nil, fmt.Errorf("Not enough users in group.")
	}
	if len(ss[len(ss)-1]) < 2 {
		ss = ss[:len(ss)-1]
	}
	names := make([]string, len(ss))
	ehps := make([]float64, len(ss))
	for i := range ss {
		cols := strings.Split(ss[i], ",")
		if len(cols) < 2 {
			return nil, nil, fmt.Errorf("Invalid data in row %v", i)
		}
		names[i] = cols[0]
		ehps[i], err = strconv.ParseFloat(cols[1], 32)
		if err != nil {
			return nil, nil, errors.WithStack(err)
		}
	}
	return names, ehps, nil
}
