package main

import (
	"fmt"
	"io"
	"io/ioutil"
	"strconv"
	"strings"

	"github.com/pkg/errors"
)

func GetRSUser(id int, name string) ([][3]int, error) {
	cache := fmt.Sprintf("user%v", id)
	res, err := GetHTTPOrCache(cache, fmt.Sprintf("http://services.runescape.com/m=hiscore_oldschool/index_lite.ws?player=%v", name))
	if err != nil {
		return nil, errors.WithStack(err)
	}
	defer func() { res.Close() }()
	stats, err := parseRS(res)
	if err != nil {
		var cerr error
		res, cerr = res.GetCached()
		if cerr == nil {
			stats, err = parseRS(res)
		}
	}
	return stats, err
}

func parseRS(r io.Reader) ([][3]int, error) {
	buf, err := ioutil.ReadAll(r)
	if err != nil {
		return nil, errors.WithStack(err)
	}
	lines := strings.Split(string(buf), "\n")
	out := make([][3]int, 0, len(lines))
	for i := 0; i < NumRSSkills; i++ {
		line := lines[i]
		if len(line) < 3 {
			continue
		}
		col := strings.Split(line, ",")
		n := [3]int{0, 0, 0}
		for i := range n {
			tmp, err := strconv.ParseInt(col[i], 10, 0)
			if err != nil {
				return nil, errors.WithStack(err)
			}
			if tmp < 0 {
				n[i] = int((^uint(0)) >> 1)
			} else {
				n[i] = int(tmp)
			}
		}
		out = append(out, n)
	}
	return out, nil
}
