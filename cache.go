package main

import (
	"io"
	"net/http"
	"os"
	"path/filepath"

	"bitbucket.org/abex/yumeko/conf"
	"github.com/pkg/errors"
)

// A simple http request lib that with failover to cached data

var cachedir string

func init() {
	conf.Config(&cachedir, "main", `
# Where to store cached data.
cache-directory=cache`)
}

type CachedRequest interface {
	io.Reader
	io.Closer
	GetCached() (CachedRequest, error)
}

func GetCached(name string) (CachedRequest, error) {
	path := filepath.Join(cachedir, name)
	return getCached(path)
}

type cached struct {
	io.ReadCloser
}

func getCached(path string) (CachedRequest, error) {
	fi, err := os.Open(path)
	return cached{fi}, errors.WithStack(err)
}

func (c cached) GetCached() (CachedRequest, error) {
	return c, nil
}

func GetHTTPOrCache(name, url string) (CachedRequest, error) {
	res, err := http.Get(url)
	if err != nil {
		cr, nerr := GetCached(name)
		if nerr != nil {
			return nil, err
		}
		return cr, nil
	}
	path := filepath.Join(cachedir, name)
	os.MkdirAll(filepath.Dir(path), 0777)
	fi, err := os.OpenFile(path+".new", os.O_WRONLY|os.O_TRUNC|os.O_CREATE, 0777)
	if err != nil {
		return nil, err
	}
	return &caching{
		Reader: io.TeeReader(res.Body, fi),
		fi:     fi,
		path:   path,
		res:    res,
	}, nil
}

type caching struct {
	io.Reader
	fi   *os.File
	path string
	res  *http.Response
}

func (c *caching) Close() error {
	c.res.Body.Close()
	c.fi.Close()
	os.Remove(c.path)
	os.Rename(c.fi.Name(), c.path)
	return nil
}

func (c *caching) GetCached() (CachedRequest, error) {
	c.res.Body.Close()
	c.fi.Close()
	return getCached(c.path)
}
