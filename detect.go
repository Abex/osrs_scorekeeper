package main

import (
	"fmt"
)

type Change struct {
	Skill     int
	Higher    int
	Displaced []int
}

func DetectChanges(sets [][2]int) (changes []Change, err error) {
	nochange := true
	for i := range sets {
		if sets[i][0] != sets[i][1] {
			sets = sets[i:]
			nochange = false
			break
		}
	}
	if nochange {
		return
	}
	for i := len(sets) - 1; i >= 0; i-- {
		if sets[i][0] != sets[i][1] {
			sets = sets[:i+1]
			break
		}
	}
	for i := range sets {
		id := sets[i][1]
		nowhigher := sets[:i]
		var washigher [][2]int
		for i := range sets {
			if sets[i][0] == id {
				washigher = sets[:i]
				break
			}
		}
		if washigher == nil {
			return nil, fmt.Errorf("Missing id %v", id)
		}
		change := Change{
			Higher: id,
		}
		for _, row := range washigher {
			v := row[0]
			found := false
			for _, row := range nowhigher {
				if row[1] == v {
					found = true
					break
				}
			}
			if !found {
				change.Displaced = append(change.Displaced, v)
			}
		}
		if len(change.Displaced) > 0 {
			changes = append(changes, change)
		}
	}
	return
}
