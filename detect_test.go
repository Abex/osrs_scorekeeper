package main

import "testing"
import "reflect"

type ts struct {
	Test   [][2]int
	Result []Change
}

var tests = map[string]ts{
	"No change": ts{
		Test: [][2]int{
			{1, 1},
			{0, 0},
			{2, 2},
			{4, 4},
		},
		Result: nil,
	},
	"Swap": ts{
		Test: [][2]int{
			{1, 1},
			{0, 2},
			{2, 0},
			{4, 4},
		},
		Result: []Change{
			{
				Higher:    2,
				Displaced: []int{0},
			},
		},
	},
	"2Swap": ts{
		Test: [][2]int{
			{1, 0},
			{0, 2},
			{2, 1},
			{4, 4},
		},
		Result: []Change{
			{
				Higher:    0,
				Displaced: []int{1},
			},
			{
				Higher:    2,
				Displaced: []int{1},
			},
		},
	},
	"3Swap": ts{
		Test: [][2]int{
			{1, 0},
			{0, 2},
			{2, 4},
			{4, 1},
		},
		Result: []Change{
			{
				Higher:    0,
				Displaced: []int{1},
			},
			{
				Higher:    2,
				Displaced: []int{1},
			},
			{
				Higher:    4,
				Displaced: []int{1},
			},
		},
	},
	"HalfSwap": ts{
		Test: [][2]int{
			{1, 2},
			{0, 0},
			{2, 1},
			{4, 4},
		},
		Result: []Change{
			{
				Higher:    2,
				Displaced: []int{1, 0},
			},
			{
				Higher:    0,
				Displaced: []int{1},
			},
		},
	},
}

func TestEverything(t *testing.T) {
	for name, test := range tests {
		t.Run(name, func(t *testing.T) {
			ar, err := DetectChanges(test.Test)
			if err != nil {
				t.Error(err)
				t.Fail()
				return
			}
			if !reflect.DeepEqual(ar, test.Result) {
				t.Errorf("Should be %v\n got %v", test.Result, ar)
				t.Fail()
			}
		})
	}
}
